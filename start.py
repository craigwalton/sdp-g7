from command import update , command
from comunications.comunications import Coms
from visionwrapper import VisionWrapper
from threading import Thread
from postprocessing.world import World
from command.strategy_controller import Strategy_controller

"""
This script launches all system required for playing a match.
Make sure RF USB stick is plugged in before running.
Remember to re-launch after half time and change side param
from left to right or vice-versa!
See command/strategy_controller.py for interactive commands
accepted.
"""

if __name__ == "__main__" :

	# parse arguments
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument("pitch", help="[0] Main pitch (3.D04), [1] Secondary pitch (3.D03)")
	parser.add_argument("team" , help="yellow or blue" )
	parser.add_argument("our" , help="our 3 dots are: pink or bright_green" )
	parser.add_argument("side" , help="which side of the pitch is ours, left or right?")
	args = parser.parse_args()

	# setup World model
	World.set_colours(args.team , args.our)
	pitch_number = int(args.pitch)
	World.set_globals(pitch_number , args.side)

	# start comms
	Coms.start_comunications()
	update.update_start()

	# start vision system in background thread
	vis = VisionWrapper(pitch=pitch_number)
	t = Thread(target = vis.run)
	t.daemon = True
	t.start()

	# start the strategy controller
	Strategy_controller.start(daemon = False)
