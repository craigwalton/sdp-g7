# Comms Protocol
This assumes the PC is the master, sending messages to Arduino as the slave. The first message should have ID 1.

# Arduino Policy
Store maxSuccessMessID, initialised to 0.
When a new message arrives:
	if (newMess.ID == maxSuccessMessID+1)
		store message
		reply OK+maxSuccessMessID
	else if (newMess.ID > maxSuccessMessID+1)
		// looks like a prev message was lost
		ignore message
		reply ER+maxSuccessMessID
	else if (newMess.ID < maxSuccessMessID+1)
		// looks like this message has been resent
		// maybe our OK message got lost
		ignore newMess
		reply OK+maxSuccessMessID

This policy ensures messages are not ever executed/stored out of order (we wouldn't want a GO > TURN to become a TURN > GO).
The Arduino only ever sends the maxSuccessMessID back to the PC, regardless of what message ID was send to Arduino.

So if we're sending maxSuccessMessID every time, what's the difference between OK and ER? ER triggers the PC to immediately resend all messages with IDs > maxSuccessMessID. OK will not cause messages with IDs > maxSuccessMessID to be resent (until their timeout expires).

# PC
Has dictionary of sentUnconfirmed messages. When it sends a message, it adds it to dictionary.
When OK+maxSuccessMessID is received, every messgae in dictionary with ID less or equal to maxSuccessMessID is deleted.
When ER+maxSuccessMessID is received from Arduino, delete any messages from dictionary with ID less or equal to maxSuccessMessID and proceed to resend all messages in dictionary, in order.
Each message in the dictionary has a sent timestamp. When a timeout is deemed to have occurred (and the message is still in the dictionary), resend all messages in dictionary.

# Instruction Format
[ID - 2 bytes][opcode - 1 byte][params - variable length][hash - 2 bytes]
All unsigned integers except params, where the encoding of number can be inferred from the opcode. The length of params can be inferred from the opcode. 
Hash is computed by summing each individual byte in the instruction's ID, opcode and params, treating them all as unsigned integers.
