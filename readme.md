This is a "tidied up" fork of Group 7's repo, aimed at providing only the essentials for re-using any of our systems. It removes legacy code (e.g. milestones) and includes additional documentation written to help 2017 students use use our systems within the [docs directory](/docs/), READMEs within individual directories and within as comments within the code itself.
See here for some video clips of the robot https://youtu.be/szvAMVbu_XM.

If you're a 2017 SDP student, I suggest you get started by reading the [user guide](/docs/user-guide-mod.pdf) (follow link, click "View Raw").

![robot.jpg](https://bitbucket.org/repo/M8q586/images/1195785629-robot.jpg)