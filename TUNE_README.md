Tuning README

#Kicking
The current disk has 28mm, 24mm and 21.75mm lobes, each with 75deg of contact. The elastic band can be loosened to adjust maximum kicking power. We need a table of what lobe size corresponds to what kicking distance for a fixed elastic band. Make sure the band is fastened the same way each time.

The PREPKICK command takes integer 0, 1 or 2. The KICK command takes integer 0, 1 or 2.

The disk location at power on must be such that the biggest lobe (2) is just touching the kicker leg, but not pushing it back.

/sdp/ardoino/instructioncc/

In PrepKickInstruction.cpp:
prepPos() determines at which earliest position at which a lobe is fully engaged with the leg. 
PREP_POS_WIDTH is the amount of positions which keep the lobe fully engaged. 
bool movingPosAcceptableForPrep() determines how early to stop the motor based on how far it's travelled (which should be proportional to its speed).

In KickInstruction.cpp:
KICKED_AFTER_PREP_POS is the number of clicks after prepPos() that the kicker will lose contact with lobe and fire.
GUARANTEED_TO_KICK is used when a Kick command is sent to an unprepared kicker, it simply turns a 1/3 of a turn to kick any strength.

#Sonar Grabber
Use /sdp/sensors/sonar/
Try not only different angles of sonar sensor housing, but also how far the grabber is allowed to open. The less it can open, the closer sensor is to ball and might work better. Let me know if we need different housing angles.

# Turning
Use /sdp/ardoino/instructioncc/

In TurnInstruction.cpp, CLICKS_TIME_PERIOD and CLICKS_TIME_SLOTS are passed to an instance of SpeedRecorder, which takes clicks and times to determine a motor speed over a period of time.

DEG_PER_CLICK is the number of degrees or robot rotation in a click. Easiest to leave in format 360.0/94.0 and adjust the 94.0.

float clicksFromDeccel() returns the number of extra clicks the robot is expected to cover after the motors are powered off due to momentum, given a speed from SpeedRecorder.

\#define TURN_TUNE to see print outs of stats for tuning. It's far more likely you need to change clicksFromDeccel() equation and not DEG_PER_CLICK. 
