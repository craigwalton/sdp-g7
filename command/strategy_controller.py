from threading import Thread
from helpers import *
from commander_team7 import Commander7
from commander_interface import Commander
from time import sleep , time

"""
This class is intended to act as an overall controller and coordinator 
for both robots.
It relies on having a commander object constructed for each class to 
which it can issue high level commands.
In Group 7's case, Commander7 (commander_team7.py) subclasses 
the abstract Commander class (commander_interface.py).
"""


class Strategy_controller(object):
	
	cont = None

	def __init__(self ):
		self.commanders = Strategy_controller.get_commanders()
		self.ended = False
		self.started = False
		self.pass_back = False
		self.our_kickof = False
		self.pennalty = False
		self.pennalty_def = False
		self.defender = False
		self.opp_kickof = False
		# set when the arduino says we have ball
		# unset when we see ball somewhere that the arduino definitely doesnt have ball
		self.hasBall = False	


    # called by start.py
	@staticmethod
	def start(daemon = True):
		if Strategy_controller.cont == None :
			cont = Strategy_controller()
			Strategy_controller.cont = cont
			t = Thread(target = cont.commanders[0].run_commander)
			t.daemon = True
			t.start()
			t = Thread(target = cont.commanders[1].run_commander)
			t.daemon = True
			t.start()
			t = Thread(target = cont.run)
			t.daemon = True
			t.start()
			if daemon:
				t = Thread(target = cont.console)
				t.daemon = True
				t.start()
			else :
				cont.console()
		else :
			print "Strategy_controller has already been started"


	# accepts certain commands typed in the terminal window used to 
	# launch start.py
	def console(self):

		inp = ""
		# type END to quit
		while inp != "END" :

			inp = raw_input("What is the input :")
			if inp == "start" or inp == "s" :
				self.started = True
			if inp == "stop" or inp == "goal" or inp == "g" :
				self.started = False
				self.commanders[0].invoke_command(self.commanders[0].stop)
				print "Strategy Paused. Type 'start' to continue."
				sleep(0.1)
			if inp == "prepkick" or inp == "prepKick" or inp == "pk" :
				self.commanders[0].Coms.prepkick(2)
			if inp == "kickoffUs" or inp == "kickoffus" or inp == "ku":
				self.our_kickof = True
			if inp == "kickoffOp" or inp == "kickoffop" or inp == "ko":
				self.opp_kickof = True
			if inp == "penaltyUs" or inp == "penaltyus" or inp == "pu" :
				self.pennalty = True
				self.started = False
			if inp == "penaltyDef" or inp == "penaltydef" or inp == "pd" or inp == "penDef" or inp == "pendef" :
				self.pennalty_def = True	# robot should face +ve y direction
				self.started = False
			if inp == "def" :
				self.defender = True
			if inp == "pass" :
				self.pass_back = True

			# commands below are for debugging/testing and should
			# not normally be used in a game situation
			if inp == "receive":
				self.commanders[0].invoke_command(self.commanders[0].receive)
			if inp == "block_goal":
				self.commanders[0].invoke_command(self.commanders[0].block_goal)
			if inp == "kickgoal":
				self.commanders[0].invoke_command(self.commanders[0].kick_for_goal)
			if inp == "getball":
				self.commanders[0].invoke_command(self.commanders[0].get_ball3)
			if inp == "getballcmd":
				world = waitForWorld(False , requireBall = True , no_oponents = 0)
				us = world.robots[0]
				ball = world.ball
				self.commanders[0].Coms.getball(us.x, us.y, us.rot, ball.x, ball.y)
			if inp == "kick2" or inp == "kick":
				self.commanders[0].Coms.prepkick(2)
				self.commanders[0].Coms.kick(2)
			if inp == "kick1":
				self.commanders[0].Coms.prepkick(1)
				self.commanders[0].Coms.kick(1)
			if inp == "kick0":
				self.commanders[0].Coms.prepkick(0)
				self.commanders[0].Coms.kick(0)
			if inp == "turn45":
				self.commanders[0].Coms.turn(45)
			if inp == "turn-45":
				self.commanders[0].Coms.turn(-45)
			if inp == "hasball":
				self.commanders[0].Coms.hasball()
			if inp == "abort":
				self.commanders[0].Coms.abort()
			if inp == "go":
				self.commanders[0].Coms.go()
			if inp == "centre":
				world = waitForWorld(False,True,0)
				us = world.robots[0]
				self.moving = True
				self.commanders[0].Coms.goxy(us.x,us.y,us.rot,150,110)
			if inp == "whohasball":
				world = waitForWorld(False,True,0)
				ball = world.ball
				print "ball owner ", ball.owner
				print "ball zone ", ball.get_zone()
				print "we last had it ",time()-ball.last_held_by_us," seconds ago"
			

	# the team's strategy rules are defined below
	def run(self):

		while not self.ended:

			# penalty taking routine
			if self.pennalty :
				self.pennalty = False
				self.commanders[0].invoke_command(self.commanders[0].kick_for_penalty)
				continue

			# penalty defence routine
			if self.pennalty_def :
				self.pennalty_def = False
				self.commanders[0].invoke_command(self.commanders[0].defend_for_penalty)
				continue

			world = waitForWorld(False,True,0)
			our_team = world.robots[0:2]
			oponents = world.robots[2:4]
			ball = world.ball
			w = world

			#if the game has not started, do nothing
			if not self.started :
				self.waitForChange()
				continue

			print "Started"
			
			# we're defender
			if self.defender :
				if self.our_kickof :
					self.our_kickof = False
					if ball.owner == -1 :
						self.commanders[0].invoke_command(self.commanders[0].get_ball2)
					else :
						self.commanders[0].invoke_command(self.commanders[0].pass_to_teammate)
					self.waitForChange()
					continue	

				elif ball.owner == 0 :
					self.commanders[0].invoke_command(self.commanders[0].pass_to_teammate)
					self.waitForChange()
					continue					
				elif ball.owner == -1 : 
					if ball.get_zone() == 0 or (ball.get_zone == 1 and dist(our_team[0].get_pos() , ball.get_pos()) <= dist(our_team[1].get_pos() , ball.get_pos())):
						self.commanders[0].invoke_command(self.commanders[0].get_ball2)
						self.waitForChange()
						continue	
					else:
						self.commanders[0].invoke_command(self.commanders[0].block_goal)
						self.waitForChange()
						continue
				else :
					self.commanders[0].invoke_command(self.commanders[0].block_goal)
					self.waitForChange()
					continue
			# we're attacker	
			else:
				# our teammate defender will take the kick off
				if self.our_kickof:
					# wait here until ball is kicked by teammate
					while world.ball.owner == 1 or world.ball.owner == 4:
						sleep(0.1)
						world = waitForWorld(False,True,0)
					print "Teammate has kicked off"
					self.our_kickof = False
					continue

				if self.opp_kickof:
					# wait here until ball is kicked by opposition
					while world.ball.owner == 2 or world.ball.owner == 3 or world.ball.owner == 4:
						sleep(0.1)
						world = waitForWorld(False,True,0)
					print "Opposition has kicked off"
					self.opp_kickof = False
					continue

				# if we've got ball
				if ball.owner == 0 : 
					# if we're showing off to the markers by passing instead of scoring
					if self.pass_back:
						self.pass_back = False
						self.commanders[0].invoke_command(self.commanders[0].pass_to_teammate)
						self.waitForChange()
						continue
					else:
						self.commanders[0].invoke_command(self.commanders[0].kick_for_goal)
						self.waitForChange()
						continue	
				# if no-one has ball	
				elif ball.owner == -1 :
					# if ball in (attacking) zone 2
					if ball.get_zone() == 2:
						self.commanders[0].invoke_command(self.commanders[0].get_ball3)
						self.waitForChange()
						continue	
					# if ball in (shared defence) zone 1 				
					elif ball.get_zone() == 1 :
						self.commanders[0].invoke_command(self.commanders[0].get_ball3)
						self.waitForChange()
						continue	
					# if our temates is on the field and the ball is in zone 0.
					elif world.robots[1] != None and world.robots[1].is_on_field()  and ball.get_zone() == 0:
						self.commanders[0].invoke_command(self.commanders[0].get_ball3)	
						self.waitForChange()
						continue
					#if they are not
					elif ball.get_zone() == 0 :
						self.commanders[0].invoke_command(self.commanders[0].get_ball3)
						self.waitForChange()
						continue
					#at this point the ball must be in zone 4, is it is not in the enemy goal, go get it	
					elif not point_in_box(ball.get_pos() , world.get_enemy_goal(False)) :
						print "Zone 4 not in goal getball"
						self.commanders[0].invoke_command(self.commanders[0].get_ball3)
						self.waitForChange()
						continue
					#if it is and our teamates is on the field
					elif world.robots[1] != None and world.robots[1].is_on_field():
						#TODO CHANGE TO INTERCEP IF TEAMMATES SHOW
						self.commanders[0].invoke_command(self.commanders[0].block_goal)	
						self.waitForChange()
						continue									
					#if they are not
					else :
						self.commanders[0].invoke_command(self.commanders[0].block_goal)
						self.waitForChange()
						continue
				# teammate has ball
				elif ball.owner == 1:
					self.commanders[0].invoke_command(self.commanders[0].receive)	
					self.waitForChange()
					continue

				# if someone else has ball and our teammate is on the field		
				elif world.robots[1] != None and world.robots[1].is_on_field() :
					#TODO CHANGE TO INTERCEP IF TEAMMATES SHOW
					self.commanders[0].invoke_command(self.commanders[0].block_goal)	
					self.waitForChange()
					continue
				#if they are not
				else :
					self.commanders[0].invoke_command(self.commanders[0].block_goal)
					self.waitForChange()
					continue
								

	def waitForChange(self, cond = None):
		world = waitForWorld(False,True,0)
		started = self.started
		ball_owner = world.ball.owner
		ball_zone = world.ball.get_zone()
		pennalty = self.pennalty
		pen_def = self.pennalty_def
		b_in_g = point_in_box(world.ball.get_pos() , world.get_enemy_goal(False))
		on_field = [robot.is_on_field() if robot else False for robot in world.robots]
		sleep(1)
		t = time()
		world = waitForWorld(False,True,0)
		while (t + 30 > time() and self.started == started and world.ball.owner == ball_owner 
			and (cond == None or cond(world)) and world.ball.get_zone() == ball_zone 
			and pennalty == self.pennalty and pen_def == self.pennalty_def 
			and on_field == [robot.is_on_field() if robot else False for robot in world.robots] 
			and b_in_g == point_in_box(world.ball.get_pos() , world.get_enemy_goal(False))):
			sleep(0.005)
			world = waitForWorld(False,True,0)
		sleep(1)
		print "Released wait for change"

		return


	def end(self):
		self.ended = True


	#ordering of command objects should corespond to the ordering of robots in word.robots
	@staticmethod
	def get_commanders():
		#neither of these have real implementations at the moment.
		return [Commander7() , Commander()]
