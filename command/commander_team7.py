from commander_interface import Commander
from helpers import * 
from postprocessing.world import World
import time

"""
This is Group 7's concrete implementation of the abstract Commander
interface.
"""

class Commander7(Commander):

	# goes to the midpoint between opposition, to block a pass
	def intercept2(self):

		if not "begun" in self.args or not self.args["begun"]:
			self.args["begun"] = True
			self.args["turning"] = False
			self.args["going"] = False

			world = waitForWorld(False , requireBall = False , no_oponents = 0)
			us = world.robots[0 if self.robot == -1 else self.robot]
			op1 = world.robots[2]
			op2 = world.robots[3]
			if op1 == None or op2 == None:
				self.args["begun"] = False
				print "Cannot intercept - need 2 opponents"
				sleep(1)
				return False

			if op1 == None or op2 == None:
				self.args["begun"] = False
				print "Cannot intercept - need 2 opponents"
				sleep(1)
				return False

			# intercept is midpoint of opposition robots
			intercept = midpoint(op1.x, op1.y, op2.x, op2.y)
			zone = point_zone(intercept[0], World.our_side == "Left")

			# not allowed in these zones
			if zone == 3 or zone == 0:
				print "Cannot intercept at point in zone ",zone
				return True

			# if close enough, don't bother turning and moving
			dist = point_dist(us.x, us.y, intercept[0] , intercept[1])
			if dist < 10:
				sleep (1)
				self.args["begun"] = False
				return False

			self.args["intercept"] = intercept

			# turn toward intercept
			C = namedtuple("C" , "x y")
			angle = us_to_obj_angle(us , C(intercept[0],intercept[1]))
			
			if abs(angle) > 10 :
				self.args["turningID"] = self.Coms.turn(angle)
				self.args["turning"] = True
				self.args["turnTime"] = time.time()

			return False

		if self.args["turning"]:
			# if not complete
			if not str(self.args["turningID"]) in self.Coms.completed_commands and time.time() - self.args["turnTime"] < 1.5:
				return False
			# if just complete
			else:
				self.args["turning"] = False
				return False

		if not self.args["going"]:
			# if there's an imminent crash
			if self.frozen:
				# restart method
				self.sleep(0.1)
				self.args["begun"] = False
				return False
			world = waitForWorld(False , requireBall = False , no_oponents = 0)
			us = world.robots[0 if self.robot == -1 else self.robot]
			# goxy
			self.args["goingID"] = self.Coms.goxy(us.x, us.y, -1, self.args["intercept"][0], self.args["intercept"][1])
			self.args["goingTime"] = time.time()
			self.args["going"] = True
			self.moving = True
			print "Commanded to go to ",self.args["intercept"]
			return False

		# if GoXY command is in swing
		if self.args["going"]:
			# if command completed by arduino
			if str(self.args["goingID"]) in self.Coms.completed_commands:
				# allow intercept() to restart
				sleep(0.5)
				self.moving = False
				self.args["begun"] = False
				return False
			# comms problem
			elif time.time() - self.args["goingTime"] > 10:
				print "apparent comms problem - no GoXY completion"
				# allow intercept() to restart
				self.moving = False
				sleep(0.5)
				self.args["begun"] = False
				return False
			# still in progress
			else:
				return False


	#Should get between the oponent holding the ball (or the ball) and the center of our goal
	def block_goal(self):
		if not "begun" in self.args or not self.args["begun"]:
			self.args["begun"] = True
			self.args["turning"] = False
			self.args["going"] = False

			world = waitForWorld(False , requireBall = True , no_oponents = 0)
			us = world.robots[0 if self.robot == -1 else self.robot]
			ball = world.ball
			ball_owner = ball.owner
			op1 = world.robots[2]
			op2 = world.robots[3]
			# can't see ball
			if ball_owner == 4:
				if op1 != None:
					block_targ = op1.get_pos()
				elif op2 != None:
					block_targ = op2.get_pos()
				else:
					self.args["begun"] = False
					self.sleep(0.5)
					return False
			# ball out somewhere in field
			elif ball_owner == -1:
				block_targ = ball.get_pos()
			# op1 has it
			elif ball_owner == 2:
				block_targ = op1.get_pos()
			# op2 has it
			elif ball_owner == 3:
				block_targ = op2.get_pos()
			else:
				self.args["begun"] = False
				self.sleep(0.5)
				return False

			goal = World.get_goal_center(True)
			targ = midpoint(block_targ[0], block_targ[1], goal[0], goal[1]) 

			zone = point_zone(targ[0], World.our_side == "Left")

			# not allowed in these zones
			if zone == 3 or zone == 0:
				print "Cannot block goal at point in zone ",zone
				self.sleep(0.5)
				self.args["begun"] = False
				return False

			# if close enough, don't bother turning and moving
			dist = point_dist(us.x, us.y, targ[0] , targ[1])
			if dist < 10:
				self.sleep (0.5)
				self.args["begun"] = False
				return False

			self.args["targ"] = targ

			# turn toward block_goal
			C = namedtuple("C" , "x y")
			angle = us_to_obj_angle(us , C(targ[0],targ[1]))
			
			if abs(angle) > 10 :
				self.args["turningID"] = self.Coms.turn(angle)
				self.args["turning"] = True
				self.args["turnTime"] = time.time()

			return False

		if self.args["turning"]:
			# if not complete
			if not str(self.args["turningID"]) in self.Coms.completed_commands and time.time() - self.args["turnTime"] < 1.5:
				return False
			# if just complete
			else:
				self.args["turning"] = False
				return False

		if not self.args["going"]:
			# if there's an imminent crash
			if self.frozen:
				# restart method
				self.sleep(0.1)
				self.args["begun"] = False
				return False
			world = waitForWorld(False , requireBall = False , no_oponents = 0)
			us = world.robots[0 if self.robot == -1 else self.robot]
			# goxy
			self.args["goingID"] = self.Coms.goxy(us.x, us.y, -1, self.args["targ"][0], self.args["targ"][1])
			self.args["goingTime"] = time.time()
			self.args["going"] = True
			self.moving = True
			print "Commanded to go to ",self.args["targ"]
			return False

		# if GoXY command is in swing
		if self.args["going"]:
			# if command completed by arduino
			if str(self.args["goingID"]) in self.Coms.completed_commands:
				# allow block_goal() to restart
				sleep(0.5)
				self.moving = False
				self.args["begun"] = False
				return False
			# comms problem
			elif time.time() - self.args["goingTime"] > 10:
				print "apparent comms problem - no GoXY completion"
				# allow block_goal() to restart
				self.moving = False
				sleep(0.5)
				self.args["begun"] = False
				return False
			# still in progress
			else:
				return False

	# # turns (if necessary) and gets ball
	# def get_ball2(self):
		
	# 	# begin by (optionally) turning
	# 	if not "begun" in self.args or self.args["begun"] == False:

	# 		# set an upper limit on the number of turns to perform
	# 		if not "turnsRemaining" in self.args:
	# 			self.args["turnsRemaining"] = 2

	# 		self.args["begun"] = True
	# 		self.args["getting"] = False
	# 		self.args["turning"] = False
	# 		self.args["turningCompTime"] = 0

	# 		# get us and ball
	# 		world = waitForWorld(False , requireBall = True , no_oponents = 0)
	# 		us = world.robots[0 if self.robot == -1 else self.robot]
	# 		ball = world.ball

	# 		# if we've already got the ball
	# 		if ball.owner == 0:
	# 			print "Not doing get_ball as we already have ball"
	# 			self.sleep(0.5)
	# 			# restart get_ball
	# 			self.args["begun"] = False
	# 			return False
			
	# 		# turn toward ball
	# 		angle = us_to_obj_angle(us , ball)
	# 		dist = point_dist(us.x, us.y, ball.x , ball.y)
	# 		# if we're close to ball, angle tolerance is less
	# 		if abs(angle) > 10 and self.args["turnsRemaining"] > 0:
	# 			self.args["turningID"] = self.Coms.turn(angle)
	# 			print "get_ball2 turn ",angle," deg"
	# 			self.args["turning"] = True
	# 			self.args["turnTime"] = time.time()
			
	# 		return False

	# 	# wait until turned, then sleep 0.5
	# 	if self.args["turning"]:
	# 		# if not complete
	# 		if not str(self.args["turningID"]) in self.Coms.completed_commands and time.time() - self.args["turnTime"] < 2:
	# 			return False
	# 		# if just complete
	# 		else:
	# 			self.sleep(0.5)
	# 			# restart get_ball
	# 			self.args["begun"] = False
	# 			return False

	# 	# if turning is complete and we haven't sent GetBall command
	# 	if not self.args["getting"]:
	# 		# if there's an imminent crash
	# 		if self.frozen:
	# 			# restart method (we're still allowed to turn)
	# 			self.sleep(0.1)
	# 			self.args["begun"] = False
	# 			return False
	# 		world = waitForWorld(False , requireBall = True , no_oponents = 0)
	# 		us = world.robots[0 if self.robot == -1 else self.robot]
	# 		ball = world.ball
	# 		# what we've all been waiting for...get ball
	# 		self.args["gettingID"] = self.Coms.getball(us.x, us.y, us.rot, ball.x, ball.y)
	# 		self.args["gettingTime"] = time.time()
	# 		self.args["getting"] = True
	# 		self.moving = True
	# 		return False

	# 	# if GetBall command is in swing
	# 	if self.args["getting"]:
	# 		# if command completed by arduino
	# 		if str(self.args["gettingID"]) in self.Coms.completed_commands:
	# 			# successfully got ball
	# 			suc = self.Coms.completed_commands[str(self.args["gettingID"])]
	# 			if suc == 1 or suc == "1":
	# 				print "We got ball!"
	# 				return True
	# 			# failed to get ball
	# 			else:
	# 				print "We failed to get ball"
	# 				self.args["begun"] = False
	# 				return False
	# 			self.moving = False
	# 		# comms problem
	# 		elif time.time() - self.args["gettingTime"] > 10:
	# 			print "apparent comms problem - no GetBall completion"
	# 			self.args["begun"] = False
	# 			self.moving = False
	# 			return False
	# 		# still in progress
	# 		else:
	# 			return False


	# turns (if necessary) and gets ball
	def get_ball3(self):

		# get us and ball
		world = waitForWorld(False , requireBall = True , no_oponents = 0)
		us = world.robots[0 if self.robot == -1 else self.robot]
		ball = world.ball
		angle_to_ball = us_to_obj_angle(us , ball)

		# if we've already got ball
		if ball.owner == 0:
			print "get_ball robot has ball"
			self.sleep(0.5)
			# restart get_ball
			self.args = {}
			return True

		# allows some states to transition immediately to another state
		# whilst keeping the same world model by calling continue.
		# states which want to wait for an event can transition to another state by returning False.
		while (1):
			
			### INITIAL STATE ###
			if not "begun" in self.args or self.args["begun"] == False:

				# set an upper limit on the number of turns to perform
				self.args["turnsRemaining"] = 2
				self.args["begun"] = True

				# immediately enter turn state
				self.args["state"] = "turn"		
				continue

			### TURN STATE ###
			if self.args["state"] == "turn":
				# if turn necessary and allowed
				if abs(angle_to_ball) > 10 and self.args["turnsRemaining"] > 0:
					self.args["turningID"] = self.Coms.turn(angle_to_ball)
					print "get_ball3 turn ",angle_to_ball," deg"
					self.args["turning"] = True
					self.args["turnTime"] = time.time()
					self.args["state"] = "turning"
					return False
				else:	# no turn allowed/necessary
					# reset turnsRemaining and immediately enter go state
					self.args["turnsRemaining"] = 2
					self.args["state"] = "go"
					continue

			### TURNING STATE ###
			if self.args["state"] == "turning":
				# if not complete
				if not str(self.args["turningID"]) in self.Coms.completed_commands and time.time() - self.args["turnTime"] < 2:
					# remain in turning state
					return False
				# if just complete
				else:
					self.sleep(0.5)
					# re-enter turn state after vision delay
					self.args["state"] = "turn"
					return False

			### GO STATE ###
			if self.args["state"] == "go":
				# if there's an imminent crash, transition to turn state
				if self.frozen:
					self.args["state"] = "turn"
					return False
				# send GetBall command
				self.args["getballID"] = self.Coms.getball(us.x, us.y, us.rot, ball.x, ball.y)
				self.args["getballTime"] = time.time()
				self.moving = True
				self.args["state"] = "going"
				return False

			### GOING STATE ###
			if self.args["state"] == "going":
				# if command completed by arduino
				if str(self.args["getballID"]) in self.Coms.completed_commands:
					suc = self.Coms.completed_commands[str(self.args["getballID"])]
					# successfully got ball
					if suc == 1 or suc == "1":
						print "We got ball!"
						self.moving = False
						# enter accepting state
						return True
					# failed to get ball
					else:
						print "We failed to get ball"
						self.sleep(0.5)
						self.moving = False
						self.args["state"] = "turn"
						return False
					self.moving = False
				# comms problem
				elif time.time() - self.args["getballTime"] > 10:
					self.moving = False
					self.args["state"] = "turn"
					return False
				# still in progress
				else:
					# remain in going state
					return False


	#Should pass the ball to their teammate (pass is a reserved keyword so that name does not work)
	def pass_to_teammate(self):
		if not "begun" in self.args or self.args["begun"] == False:
			self.args["begun"] = True
			self.args["turnsRemaining"] = 4
			self.args["turning"] = False

			world = waitForWorld(True , requireBall = False , no_oponents = 0)
			us  = world.robots[0 if self.robot == -1 else self.robot]
			mate = world.robots[1]
			angle = us_to_obj_angle(us , mate)

			if abs(angle) > 5 and self.args["turnsRemaining"] > 0:
				self.args["turnID"] = self.Coms.turn(angle , 1)
				self.args["turning"] = True
				self.args["turnTime"] = time.time()
				self.args["turnsRemaining"] = self.args["turnsRemaining"] - 1
			return False

		if self.args["turning"]:
			# if not complete
			if not str(self.args["turnID"]) in self.Coms.completed_commands and time.time() - self.args["turnTime"] < 1.5:
				return False
			# if just complete
			else:
				self.sleep(0.5)
				# restart pass_to_teammate
				self.args["begun"] = False
				return False
		
		self.Coms.prepkick(1)
		self.Coms.kick(1)
		self.Coms.prepkick(2)
		return True

	#Robot should get ready to receive the ball, aka open grabers and turn to team mate
	#Should set self.ready to true when it is ready to receive the ball
	def receive(self):
		if not "begun" in self.args or self.args["begun"] == False:
			self.args["begun"] = True
			self.args["receiving"] = False
			self.args["turning"] = False
			world = waitForWorld(True , requireBall = False , no_oponents = 0)
			us  = world.robots[0 if self.robot == -1 else self.robot]
			mate = world.robots[1]
			angle = us_to_obj_angle(us , mate)
			if abs(angle) > 15:
				self.args["turnID"] = self.Coms.turn(angle , 1)
				self.args["turnTime"] = time.time()
				self.args["turning"] = True
			return False

		if self.args["turning"]:
			# if not complete
			if not str(self.args["turnID"]) in self.Coms.completed_commands and time.time() - self.args["turnTime"] < 1.5:
				return False
			# if just complete
			else:
				self.args["turning"] = False
				return False

		if not self.args["receiving"]:
			self.args["receiving"] = True
			self.args["receiveTime"] = time.time()
			self.args["receiveID"] = self.Coms.receive(5000)
			return False

		# if Receive command is in swing
		if self.args["receiving"]:
			# if command completed by arduino
			if str(self.args["receiveID"]) in self.Coms.completed_commands:
				# successfully received ball
				suc = self.Coms.completed_commands[str(self.args["receiveID"])]
				if suc == 1 or suc == "1":
					print "We received ball!"
					return True
				# failed to receive ball
				else:
					self.args["begun"] = False
					return False
			# comms problem
			elif time.time() - self.args["receiveTime"] > 6:
				print "apparent comms problem - no Receive completion"
				self.args["begun"] = False
				return False
			# still in progress
			else:
				return False
				
		return False
	

	#should turn to get a clear shot of the oponents goal and then kick for it
	def kick_for_goal(self):

		# initial phase - turn if necessary
		if not "begun" in self.args or self.args["begun"] == False:
			self.args["begun"] = True
			self.args["turning"] = False
			self.args["kicking"] = False
			if not "turnsRemaining" in self.args:
				self.args["turnsRemaining"] = 4

			world = waitForWorld(False , requireBall = False , no_oponents = 0)
			us  = world.robots[0 if self.robot == -1 else self.robot]
			target = World.get_goal_center(False)

			C = namedtuple("C" , "x y")
			angle = us_to_obj_angle(us ,C(target[0],target[1]))

			if abs(angle) > 5 and self.args["turnsRemaining"] > 0:
				self.args["turnID"] = self.Coms.turn(angle , 0)
				self.args["turning"] = True
				self.args["turnTime"] = time.time()
				self.args["turnsRemaining"] = self.args["turnsRemaining"] - 1
			return False

		# wait for turn to complete phase
		if self.args["turning"]:
			# if not complete
			if not str(self.args["turnID"]) in self.Coms.completed_commands and time.time() - self.args["turnTime"] < 1.5:
				return False
			# if just complete
			else:
				self.sleep(0.5)
				# restart kick_for_goal
				self.args["begun"] = False
				return False
				
		# send prepkick and kick phase
		if not self.args["kicking"]:
			self.Coms.prepkick(2)
			self.args["kicking"] = True
			self.args["kickTime"] = time.time()
			self.args["kickID"] = self.Coms.kick(2)
			self.Coms.prepkick(2)
			return False
		else:
			# if command completed by arduino
			if str(self.args["kickID"]) in self.Coms.completed_commands:
				# failed to kick ball (we still have it)
				suc = self.Coms.completed_commands[str(self.args["kickID"])]
				print "suc ",suc
				if suc == 1 or suc == "1":
					print "We failed to kick ball"
					self.args["kicking"] = False
					return False
				# kicked ball
				else:
					print "We managed to kick ball ",suc
					return True
			# comms problem
			elif time.time() - self.args["kickTime"] > 8:
				print "apparent comms problem - no Kick completion"
				self.args["kicking"] = False
				return False
			# still in progress
			else:
				return False

		return False

	def kick_for_penalty(self):
		print "kick_for_penalty"
		if not "begun" in self.args:

			self.args["begun"] = True

			# prepare for kick immediately
			self.Coms.prepkick(2)

			# turn to dummy (closed) corner, to entice opponent further into that corner
			world = waitForWorld(False , requireBall = False , no_oponents = 1)
			us  = world.robots[0 if self.robot == -1 else self.robot]
			goaliePos = getPenaltyGoaliePos(us, world.robots[2], world.robots[3])

			goal_x_coor = 300 if us.get_pos()[0] > 150 else 0
			self.args["goal_x_coor"] = goal_x_coor
			dummy_corner_y_coor = 140 if goaliePos[1] > 110 else 80
			dummy_targ = array((goal_x_coor,dummy_corner_y_coor))

			C = namedtuple("C" , "x y")
			to_dummy_angle = us_to_obj_angle(us ,C(dummy_targ[0],dummy_targ[1]))
			print "kick_for_penalty dummy y-coor ",dummy_corner_y_coor
			self.Coms.turn(to_dummy_angle , 1)

			# confuse the enemy and spectators
			self.Coms.grab(1)
			self.Coms.grab(0)

			self.sleep(4)
			return False

		# turn to most open corner
		world = waitForWorld(False , requireBall = False , no_oponents = 1)
		us = world.robots[0 if self.robot == -1 else self.robot]
		goaliePos = getPenaltyGoaliePos(us, world.robots[2], world.robots[3])

		southSpace = 140 - goaliePos[1]
		northSpaceSpace = goaliePos[1] - 80

		# shoot north (low y-coor)
		if goaliePos[1] > 110:
			robotNorthExtremity = goaliePos[1] - 10
			avg = (robotNorthExtremity + 80)/2
			# shoot no further south than goal centre
			shoot_corner_y_coor = min(avg, 110)
		else:	# shoot south (high y-coor)
			robotSouthExtremity = goaliePos[1] + 10
			avg = (140 + robotSouthExtremity)/2
			# shoot no further north then goal centre
			shoot_corner_y_coor = max(avg, 110)
		
		print "kick_for_penalty shoot at y-coor",shoot_corner_y_coor

		shoot_targ = array((self.args["goal_x_coor"],shoot_corner_y_coor)) 
		C = namedtuple("C" , "x y")
		to_shoot_angle = us_to_obj_angle(us ,C(shoot_targ[0],shoot_targ[1]))
		self.Coms.turn(to_shoot_angle , 1)
		
		self.Coms.kick(2)
		
		return True


	def defend_for_penalty(self):

		# only send pendef command once
		if not "begun" in self.args or self.args["begun"] == False:
			self.args["begun"] = True
			self.Coms.pendef()

		world = pp.world.World.get_world()
		# custom implementation of waitForWorld() where us is not required
		while world == None or (world.robots[2] == None and world.robots[3] == None):
			world = pp.world.World.get_world()
		penTaker = getPenaltyTakerPos(World.our_side == "Left", world.robots[2], world.robots[3])
		
		# find place to intercept shot on goalline
		if penTaker != None:
			if World.our_side == "Left":
				intercept = penTaker.y - tan(radians(penTaker.rot))*penTaker.x
			else:
				intercept = penTaker.y - tan(radians(penTaker.rot))*(penTaker.x-330)
			print "penTaker ",penTaker.get_pos()
			print "penTaker head ",penTaker.rot
		else:
			intercept = 110
		
		print "intercept ",intercept
		interceptRelativeToCentre = intercept - 110
		# limit to sensible values incase of 90deg aim
		if interceptRelativeToCentre > 110:
			interceptRelativeToCentre = 110
		if interceptRelativeToCentre < -110:
			interceptRelativeToCentre = -110
		print "interceptRelativeToCentre ",interceptRelativeToCentre
		self.Coms.pendefupd(interceptRelativeToCentre)
		
		self.sleep (0.5)

		return False


	#Should cancel all actions currently being executed by the robot
	#Main use will be if the situation has changed in such a way that our goal can not be acheived
	#This should leave the robot in a safe, neutral state as much as possible
	def stop(self):
		self.Coms.abort()
		self.sleep(0.2)
		return True
