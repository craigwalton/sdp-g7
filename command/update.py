from postprocessing.world import World
from comunications.comunications import Coms
from time import sleep , time
from threading import Thread

"""
This script runs in a background thread and is responsible for 
asking Coms to send the robot a world model update at regular
intervals.
"""

def update_start():

	t = Thread(target = update_loop)
	t.daemon = True
	t.start()

#Primary loop of the update thread, resends the position of the robot at regular intervals
def update_loop() :
	start_time = int(time()*1000 )
	last_robot_update = start_time
	count = 0
	while True :
		world = World.get_world()
		#Ensure that all the data we need is there and that the data is fresh
		#If the vison system has droped the robot then we skip sending an update
		if world != None and world.robots[0] != None and world.robots[0].t != last_robot_update:
			us = world.robots[0]
			last_robot_update = us.t
			Coms.updatewm(int(1000*world.time) - start_time, us.projectedX() , us.projectedY() , us.rot)

		sleep(0.3)


