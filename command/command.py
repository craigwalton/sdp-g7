from postprocessing.world import World 
from comunications.comunications import Coms
from math import atan2 , pi , sqrt , cos , sin , radians , pow  , isinf , isnan
from time import sleep , time
from numpy import * 
from numpy import linalg as la
import numpy as np
from collections import namedtuple
from helpers import * 

"""
This script is not used during a match. It was mostly used for 
testing and debugging and for milestone tasks.
"""

def start() : 
	inp = ""

	while inp != "END" :
		inp = raw_input("Which trial are we on? :")
		if inp == "" :
			print "Empty input resending: " + lastinp
			inp = lastinp
		else :
			lastinp = inp
		if inp == "1" :
			trial_one()
		elif inp == "2" :
			trial_two()
		elif inp == "3" :
			trial_three()
		elif inp == "4":
			trial_four()
		elif inp == "5" :
			shoot_at(0 , 120)
		elif inp == "6":
			shoot_at(300 , 120)
		elif inp == "10":
			course_correct()
		elif inp == "11":
			get_ball_instruction()
		elif inp == "12":
			turn_get_ball()
		elif inp == "13":
			get_ball()
		elif inp == "101":
			ms3_trial_one()
		elif inp == "102":
			ms3_trial_two()
		elif inp == "103":
			ms3_trial_three1()
		elif inp == "104":
			ms3_trial_three2()
		elif inp == "DBUGIBH":
			debug_is_ball_held()
		elif inp == "DBUGIOP":
			debug_is_on_pitch()
		elif inp =="DBUGINT":
			debug_interception()
		elif inp == "DBUGMS32":
			debug_ms3_2()
		elif inp == "STOP":
			Coms.stop()
		elif inp == "ABORT":
			Coms.abort()
		elif inp == "RESET":
			Coms.reset()
		elif inp == "HASBALL":
			Coms.hasball()
		#else:
			#Coms.send_message(inp + '\r')


def ms3_trial_one():

	# loops until world available
	world = waitForWorld(True , requireBall = False)
	
	turnToObj(world.robots[1], ardCorrectionsAllowed =  0 , pcAttemptsRemaining = 1)

	# TODO: determine if ball was successfully grabbed
	Coms.receive(10000)
	
	sleep(11)
	Coms.stop()

	v = isBallHeld()
	print v
	if isBallHeld() == -1:
		get_ball()


def turnToObj(obj, ardCorrectionsAllowed = 0, pcAttemptsRemaining =1):

	world = waitForWorld(False)
	while abs(us_to_obj_angle(world.robots[0] , obj)) > 10 and pcAttemptsRemaining >= 1:
		pcAttemptsRemaining = pcAttemptsRemaining -1
		turn = us_to_obj_angle(world.robots[0], obj)
		Coms.turn(turn , ardCorrectionsAllowed)
		sleep(0.5 * (ardCorrectionsAllowed +1) + 0.5)
		world = waitForWorld(False)

def turnToBall(attemptsRemaining):
	"""
	Specific implementation of turnToObj turning towards the ball that handles that the ball might be moving
	"""
	world = waitForWorld(False)
	#while us_to_obj_angle(world.ball, world.robots[0]) > 10:
	while abs(us_to_obj_angle(world.robots[0], world.ball)) > 10 and attemptsRemaining >= 1:
		attemptsRemaining = attemptsRemaining - 1
		Coms.stop()
		turn = us_to_obj_angle(world.robots[0], world.ball)
		# if robot needs to turn, command it to
		# then re-enter function to check again
		Coms.turn(turn , 1)
		sleep(2)
		
		world = waitForWorld(False)

def get_ball():
	"""
	Gets the ball like normal if the ball is not close to a wall, otherwise it will
	attempt to aproach the wall at a 90 degree angle, grab the ball and then back off
	"""

	world = waitForWorld(False)

	pitch_box = World.get_pitch().get_inner_box()
	ballp = array((world.ball.x, world.ball.y))
	usp = array((world.robots[0].x , world.robots[0].y)) 
	if not point_in_box(ballp , pitch_box) and point_in_box(usp, pitch_box):
		print "trigger"
		p1 , p2 = find_closest_edge(ballp ,  pitch_box)
		inter = calculate_intercept_p(p1 , p2 , ballp)
		point = inter + set_magnitude( inter - ballp , 15) 

		C = namedtuple("C" , "x y")
		turnToObj(C(point[0], point[1]) , 0 , 1)
		Coms.goxy(usp[0],usp[1],-1,point[0],point[1])
		print point
		print ballp
		sleep(dist(point, usp)/30.0 + 0.5)
		turnToBall(3)

		get_ball_instruction()
		#Stop incase the robot is just driving into a wall
		Coms.stop()
		Coms.reverse(20)
		sleep(0.7)
	else:
		turnToBall(3)
		get_ball_instruction()




def ms3_trial_two():
	# command robot to receive ball
	

	Coms.receive(10000)

	# TODO: determine if ball was successfully grabbed
	sleep(11)


	if isBallHeld() == -1:
		get_ball()

	


	world = waitForWorld(True)
	turnToObj(world.robots[1], ardCorrectionsAllowed = 0, pcAttemptsRemaining = 4)
	
	Coms.prepkick(2)
	sleep(1)
	Coms.kick(2)

def ms3_trial_three2():
	world = waitForWorld(requireBall = False , no_oponents = 1)

	us = world.robots[0]

	op1 = world.robots[3]
	tmp = world.robots[2]

	if op1 == None or tmp.t > op1.t :
		op1 = tmp



	op1p = array((op1.x , op1.y))

	usp = array((us.x,us.y))


	if (dist(usp , array((0,110))) < dist(usp, array((300,110)))) :
		op2p = array((0,110))
	else :
		op2p = array((300,110))
	goals = World.get_goals()
	goals = sorted(goals , key = lambda g : dist(g.get_center() , usp))
	goal = goals[0]
	box = goal.get_inner_box()

	point = calculate_intercept_p(op1p, op2p , usp)

	#find interception point as close to kicker as possible
	a = find_intersecting_edge(goal.get_center() , op1p, box)
	if a != None:
		p1 , p2 , _ = a
		point = seg_intersect(op2p , op1p , p1 , p2)
	else :
		print "None in ms3_2"

	C = namedtuple("C" , "x y")
	angle= us_to_obj_angle(us,C(point[0], point[1]) )
	
	Coms.turn(angle)
	sleep(0.5)
	Coms.goxy(us.x , us.y, -1 ,int(point[0]) , int(point[1]))
	usp = array((us.x,us.y))
	distance = dist(usp , point)
	sleep(distance/30.0 + 0.5)

	world = waitForWorld(requireBall = False)
	us = world.robots[0]

	usp = array((us.x,us.y))
	angle= us_to_obj_angle(us,op1)


	Coms.turn(45 +angle)



def ms3_trial_three1():

	world = waitForWorld(False ,requireBall = False ,  no_oponents = 2)

	us = world.robots[0]


	op1 = world.robots[2]
	op2 = world.robots[3]

	op1p = array((op1.x , op1.y))
	op2p = array((op2.x , op2.y))	
	usp = array((us.x,us.y))

	goals = World.get_goals()
	goals = sorted(goals , key = lambda g : dist(g.get_center() , usp))
	goal = goals[0]
	box = goal.get_outer_box()



	if point_in_box(op1p , box) :
		tmp = op2p
		op2p = op1p
		op1p = tmp


	#find an interception point
	point = calculate_intercept_p(op1p, op2p , usp)
	if point_in_box(point , box) :
		for i in range(0,4):
			p1 = box[i]
			p2 = box[(i+1)%4]
			intersect = seg_intersect(point , op1p , p1 , p2 )
			if not isnan(intersect[0])  and not isinf(intersect[0]) and (point_between(intersect , op1p , op2p) ):
				point = intersect
				break

	print point
	if point_in_box(usp , box):
		edge = find_intersecting_edge(World.get_pitch().get_center() , goal.get_center() , box)
		edge = (edge[0] , edge[1])
		closest = min(edge , key = lambda x : dist(x , usp))
		path = calculate_path_around(closest , point , box)
		path.insert(0 , closest )
	else :
		path = calculate_path_around(usp , point , box)

	if len(path) > 1 and path[0].tolist() == path[1].tolist():
		path.pop(0)


	
	C = namedtuple("C" , "x y")

	print path
	#travel to the interception point
	for point in path:
		world = waitForWorld(False , requireBall = False)
		us = world.robots[0]
		angle= us_to_obj_angle(us,C(point[0], point[1]) )
		Coms.turn(angle)
		sleep(0.5)
		Coms.goxy(us.x , us.y, -1 ,int(point[0]) , int(point[1]))
		usp = array((us.x,us.y))
		distance = dist(usp , point)
		sleep(distance/30.0 + 0.5)

	#turn towards our oponent.

	world = waitForWorld(False , requireBall = False)
	us = world.robots[0]
	angle= us_to_obj_angle(us,op2)


	Coms.turn(45 +angle)


def debug_ms3_2():
	while True :
		world = waitForWorld(False , requireBall = False , no_oponents = 1)

		us = world.robots[0]

		op1 = world.robots[3]
		tmp = world.robots[2]
		print op1.t
		print tmp.t

		if op1 == None or tmp.t > op1.t :
			op1 = tmp


		op1p = array((op1.x , op1.y))

		usp = array((us.x,us.y))


		if (dist(usp , array((0,110))) < dist(usp, array((300,110)))) :
			op2p = array((0,110))
		else :
			op2p = array((300,110))
		goals = World.get_goals()
		goals = sorted(goals , key = lambda g : dist(g.get_center() , usp))
		goal = goals[0]
		box = goal.get_inner_box()

		point = calculate_intercept_p(op1p, op2p , usp)

		#find interception point as close to kicker as possible
		a = find_intersecting_edge(goal.get_center() , op1p, box)
		if a != None:
			print "None in ms3_2"
			p1 , p2 , _ = a
			point = seg_intersect(op2p , op1p , p1 , p2)
		print point
		sleep(1)


def debug_is_ball_held() :
	while True :
		print isBallHeld()
		sleep(0.5)

def debug_is_on_pitch():
	while True:
		world = waitForWorld(False , requireBall = False)

		pitch_box = World.get_pitch().get_inner_box()
		ballp = array((world.ball.x, world.ball.y))
		usp = array((world.robots[0].x , world.robots[0].y)) 
		left_goal = World.get_goals()[0]
		right_goal = World.get_goals()[1]
		print "PITCH " + str(point_in_box(usp , pitch_box))
		print "LEFT INNER " + str(point_in_box(usp , left_goal.get_inner_box()))
		print "LEFT OUTER " + str(point_in_box(usp , left_goal.get_outer_box()))
		print "RIGHT INNER " + str(point_in_box(usp , right_goal.get_inner_box()))
		print "RIGHT OUTER " + str(point_in_box(usp , right_goal.get_outer_box()))
		sleep(0.5)

def debug_interception() :
	while True :

		world = waitForWorld(False , requireBall = False , no_oponents = 2)

		us = world.robots[0]


		op1 = world.robots[2]
		op2 = world.robots[3]

		op1p = array((op1.x , op1.y))
		op2p = array((op2.x , op2.y))	
		usp = array((us.x,us.y))

		goals = World.get_goals()
		goals = sorted(goals , key = lambda g : dist(g.get_center() , usp))
		goal = goals[0]
		box = goal.get_outer_box()



		if point_in_box(op1p , box) :
			tmp = op2p
			op2p = op1p
			op1p = tmp



		point = calculate_intercept_p(op1p, op2p , usp)

		#find an interception point
		if point_in_box(point , box) :
			for i in range(0,4):
				p1 = box[i]
				p2 = box[(i+1)%4]
				intersect = seg_intersect(point , op1p , p1 , p2 )
				if not isnan(intersect[0])  and not isinf(intersect[0]) and (point_between(intersect , op1p , op2p) ):
					point = intersect
					break


		if point_in_box(usp , box):
			edge = find_intersecting_edge(World.get_pitch().get_center() , goal.get_center() , box)
			edge = (edge[0] , edge[1])
			print box
			print edge
			closest = min(edge , key = lambda x : dist(x , usp))
			path = calculate_path_around(closest , point , box)
			path.insert(0 , closest )
			print "closest " + str(closest)
		else :
			path = calculate_path_around(usp , point , box)

		if len(path) > 1 and path[0].tolist() == path[1].tolist():
			path.pop(0)

		
		C = namedtuple("C" , "x y")

		print path
		sleep(1)



def get_ball_instruction():
	world =  World.get_world()
	while world == None or world.robots[0] == None or world.ball == None:
		world = World.get_world()
	Coms.getball(world.robots[0].projectedX(), world.robots[0].projectedY(), world.robots[0].rot, world.ball.x, world.ball.y);
	distance = point_dist(world.robots[0].x , world.robots[0].y , world.ball.x , world.ball.y)
	sleep(distance/30.0 + 0.5)

def turn_get_ball():
	world = waitForWorld(False)
	turnToObj(world.ball, 1)
	world = waitForWorld(False)
	Coms.getball(world.robots[0].projectedX(), world.robots[0].projectedY(), world.robots[0].rot, world.ball.x, world.ball.y);


def course_correct():
	world =  World.get_world()
	while world == None or world.robots[0] == None or world.ball == None:
		world = World.get_world()
	Coms.grab(1)
	Coms.goxy(world.robots[0].projectedX(), world.robots[0].projectedY(), world.ball.x, world.ball.y);
	Coms.grab(0)
	
def trial_one():
	Coms.go(50)
	sleep(2)
	Coms.stop()
	get_ball(True)

def trial_two():
	get_ball(False)

def trial_three() :
	shoot_at(0 ,110)

def trial_four():
	shoot_at(300 , 100)



