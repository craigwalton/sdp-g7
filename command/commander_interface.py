from time import time , sleep
from helpers import * 
import comunications.comunications as coms
from math import atan2 , pi , sqrt , cos , sin , radians , pow  , isinf , isnan
from numpy import * 
from numpy import linalg as la
from postprocessing.world import World


""""
This defines a basic Abstract class for interaction between the planner and the robots / coms
These methods should not take more than a few milliseconds to execute each time, if a sequence of 
actions are required, maintain that state inside the args dictionary.
Any methods that require arguments will have those passed as part of the args dict

The commander is responsible for obstacle avoidance. calculate_path can be used to calculate a path that 
should not colide with any robots. Each commander is also responsible for not entering the oponents goal zone
point_in_box can be used to check if a target destination is inside an enemy goal zone
"""


class Commander(object):
	
	def __init__(self , robot = -1):
		self.current_command = None
		self.args = {}
		self.ended = False
		self.last_command_finished = True
		self.ready = False
		self.sleep_until = 0
		self.new = None
		self.frozen = False
		self.robot = robot
		self.moving = False # Set to true every time a command that moved the robot forwards is triggered
		self.Coms = coms.Coms.com


	#Should be called in a thread that is allowed to loop forever in this method (aka a new one)
	def run_commander(self):
		print "Started commander for " + str(self.robot)
		while not self.ended:

			#This part sets slelf.frozen to true if we risk crashing into someone if we move along at full speed
			world = waitForWorld(False, False , 0)
			us = world.robots[0]
			
			#Calculate a 50 cm long line from us to 50 cm ahead of us
			dir_r = (cos(radians(us.rot)) , sin(radians(us.rot)))
			usp = array((us.x, us.y))
			end = usp + set_magnitude(dir_r , 50)
			end_2 = usp + set_magnitude(dir_r , 30)
			val = False
			for i , robot in enumerate(world.robots[1:]):
				if robot == None:
					continue
				center = array((robot.x , robot.y))

				#Calculate the point on that line closest to the robot
				a_to_p = center - usp
				a_to_b = end - usp
				atb2 = pow(la.norm(a_to_b), 2)
				atpdatb = dot(a_to_p,a_to_b)
				t = atpdatb / atb2
				closest = usp + a_to_b*t

				#if the point is less than 30 cm from the line and the point is on th 50 cm line segment
				if dist(center , closest) < 15 and point_between(closest , usp , end) and robot.is_on_field():
					#print "Robot crash " , i+1 ,usp , us.is_on_field() , center , closest
					val = True
					break

			# crash into wall
			if not point_in_box(end_2 , World.get_pitch().points) :
				#print "Wall crash"
				val = True

			if val :
				self.frozen = True

			if val and self.moving :
				self.stop()
				print "Crash - stopping"
				self.moving = False
				# give abort command the chance to pause and determine if we have ball
				# arduino needs 500ms to drop grabber, give some extra time for comms delay
				self.sleep(1.0)
				self.args = {}

			if not val  and self.frozen :
				#if we are not longer at risk of colision
				#restart current command.
				self.frozen = False



			#if we have a command that is not finished and we are not sleeping and not at risk of colision
			if self.current_command != None and not self.last_command_finished and time() >= self.sleep_until :
				self.last_command_finished =  self.current_command()

			sleep(0.005)
			if self.new != None and (self.new != self.current_command or self.last_command_finished):
				self.stop()
				sleep(0.01)
				print "NEw command for " + str(self.robot) + " " + self.new.__name__
				if self.nargs != None:
					self.args = self.nargs
					self.nargs = None
				self.current_command = self.new 
				self.new = None
				self.sleep_until = 0
				self.last_command_finished = False
				self.frozen = False
				self.moving = False
			else :
				self.new = None
				self.nargs = None



	#Used to invoke a command on a commander
	def invoke_command(self , command , args = None):
		print "Invoke for " + str(self.robot) + " " + command.__name__
		self.ready = False
		self.sleep_until = 0
		#Bussywait untill we have consumed the next command, should take at most 0.001 seconds
		while self.new != None :
			sleep(0.005)
		self.new = command
		if args != None :
			self.nargs = args
		else :
			self.nargs = {}

	#Send the commander to sleep for sleep_for seconds (give as a decimal for smaller time intervals)
	#without freezing the thread, this means that the strategy system can still send it commands. 
	def sleep(self,sleep_for):
		self.sleep_until = time() + sleep_for

	#Method for killing the commander, I'm not sure if it will ever be used. 
	def end(self):
		self.ended = True

	#Should endevor to keep the robot between the two oponents as much as possible
	def intercept(self):
		return True

	#Should get between the oponent holding the ball (or the ball) and the center of our goal
	def block_goal(self):
		return True

	#Should (as quickly as possible) cause the robot to go get the ball
	def get_ball(self):
		return True

	#Should pass the ball to their teammate (pass is a reserved keyword so that name does not work)
	def pass_to_teammate(self):
		return True

	#Robot should get ready to receive the ball, aka open grabers and turn to team mate
	#Should set self.ready to true when it is ready to reecieve the ball
	def receive(self):
		return True

	#should manuver to get a clear shot of the oponents goal and then kick for it
	def kick_for_goal(self):
		return True

	#should (optionally) turn and shoot
	#assume robot is perpendicular to goal line
	def kick_for_penalty(self):
		return True

	#Should head for x , y as quickly as possible (x,y passed in the args dict)
	#might not be needed, we'll see
	def goto(self):
		return True

	#Should cancel all actions currently being executed by the robot
	#Main use will be if the situation has changed in such a way that our goal can not be acheived
	#This should leave the robot in a safe, neutral state as much as possible
	def stop(self):
		return True
