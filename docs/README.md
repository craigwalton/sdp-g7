This directory contains documents written with the purpose of aiding a 2017 SDP Group use, copy, or just learn from any of our systems or hardware. 

The User Guide and Technical Specification are modified copies of the reports we submitted for marking but with less "can we have marks please?" and more honest and useful information. The User Guide tells you what you need to do to run our systems and maintain the robot, whereas Tech Spec contains a "bird's eye view" of our systems and some insight into their functionality. The original reports we submitted in April 2016 can be found in the /docs/originals/ directory.

The three main hardware design aspects are covered in their individual documents, which will be useful if you wish to base your robot on any of these aspects, or attempt to outright copy (very much allowed) our robot:

	*	kicking-mechanisn.pdf
	*	grabbing-mechanism.pdf
	*	movement.pdf

The vision system tuning is decribed within the User Guide and also in more detail in vision-calibration.pdf.